(ns blablavoice.trip
  (:use [clojure.pprint :as pp])
  (:require [blablavoice.parser :as parser]
            [clj-http.client :as http]
            [cheshire.core :as json]
            [clojure.tools.trace :as ct]
            [clojure.data.codec.base64 :as b64]))

(def base "https://api.blablacar.com/api/v2/")
(def access-token "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE0Njg2OTMwNTAsImNsaWVudCI6ImFwaV90ZXN0X2lkIiwic2NvcGUiOiIiLCJ1c2VyIjpudWxsfQ.WwlYDxI6C64YxhHTVc-CW1yduBwgrLmuAo8lqW3-uX4")

;(defn find-trip [str]
;   (parser/parse-message str))

;; TODO: put these in util



;; TODO after parsing send the results as a requestb

(defn parse-int [s]
  (Integer. (re-find #"[0-9]*" s)))

(defn price-to-int [str-price]
  (map read-string (re-seq #"[\d.]+" str-price)))

(defn parse-price [price-arr]
  (price-to-int (first price-arr)))

;; GET http://api.blablacar.com/v2/geocode
(defn geocode [access-token address]
  (-> (http/get (str base "geocode")
                {:query-params {:access_token access-token
                                :address      address}})
      :body
      json/parse-string))

(defn pt-coord [geocode-result name]
  (format "%.3f" (get-in geocode-result ["results" 0 "geometry" "location" name])))

;; for now we're doing it two times -> TODO: refactor it
(defn coordinates [access-token address]
  (let [gc (geocode access-token address)]
    (str (pt-coord gc "lat")
         "|" (pt-coord gc "lng"))));; we may need to truncate them

;; let's see if parse-message gets us all data needed by trips;; for now we arent supporting any roundtrip & only price max
(defn trips [access-token from-name to-name date-begin price-max price-min]
  (-> (http/get (str base "trips")
                (ct/trace {:query-params {:access_token access-token
                                          :_format "json"
                                          :locale "en_GB"
                                          :cur "GBP"
                                          :fn from-name
                                          :tn to-name
                                          :db date-begin
                                          :pmax price-max
                                          :pmin price-min}}))
                                         ;:hb           hour-begin ;; add it as an optional
      :body
      json/parse-string))

(defn tripfind-parsed [result]
  (pprint " Im here 1")
  (trips access-token
         (first (:locations result))
         (second (:locations result))
         (:starts-at result)
         (parse-price (:price result))
         (parse-price (:price result))))

;; this causes pbms when there's no access token
(defn find-trip [message]
  (pprint (str " Im here 2 & str to parse " message))
  (tripfind-parsed (parser/parse-message message)))


; (def gv (geocode access-token "Paris"))
; (get-in (geocode access-token "Paris") ["results" 0 "address_components"])
;
; (get-in gv ["results" 0 "geometry" "location" "lat"])
; (get-in gv ["results" 0 "geometry" "location" "lng"])


; @Query(ACCESS_TOKEN) String accessToken, @Query("address") String input
; exple that works
; (find-trip "Reserve a trip with John Oliver on 21 June 2016 at 11pm London to Manchester for $ 40")
; take the data from this and do an api call to blablacar api -> how ?
; then redirect that result back to user with one response


;(defn string-to-base64-string [original] (String. (b64/encode (.getBytes original)) "UTF-8"))
;(string-to-base64-string "01194c6a7055f0e6d1c3d71ecf520059e7692425d27c66dee3c621bf5bfa22bd:d70b3146a17443e2b6beade49c3eafbc65dfa88ec6d0800a67e935a47336cb96")
