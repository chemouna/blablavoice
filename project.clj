(defproject blablavoice "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [instaparse "1.4.2"]
                 [clojure-opennlp "0.3.3"]
                 [clj-time "0.12.0"]
                 [compojure "1.5.0"]
                 [ring "1.5.0"]
                 [ring/ring-json "0.4.0"]
                 [com.taoensso/timbre "4.4.0"]
                 [clj-http "2.2.0"]
                 [cheshire "5.6.1"]
                 [org.clojure/clojurescript "1.9.36"]]
  :ring {:handler blablavoice.core/app
         :auto-reload? true
         :auto-refresh? true}
  :plugins [[lein-ring "0.9.7"]])
